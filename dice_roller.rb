# frozen_string_literal: true

require './die.rb'

def check_die_format(die_string)
  (die_string =~ /\d+d\d+/ ? true : false)
end

def get_num_dice_and_sides(die_string)
  num_dice = die_string.match('(^(\d+))(?=d)')
  num_sides = die_string.match('(?<=d)(\d+)')
  [num_dice.to_s.to_i, num_sides.to_s.to_i]
end

def roll_dice(num_dice, die)
  sum = 0
  i = 0
  while i < num_dice
    print ', ' unless i.zero?
    roll = die.roll()
    sum += roll
    print roll
    i += 1
  end
  puts "\nSUM: " + sum.to_s
end

def roll_new_character
  die = Die.new(6)
  (1..6).each do |_i|
    stat = die.roll_stat
    mod = find_modifier(stat)
    puts "\nstat: " + stat.to_s + ', mod: ' + mod.to_s
  end
end

def find_modifier(stat)
  ((stat - 10) / 2).floor
end

if ARGV.length == 1
  if check_die_format(ARGV[0])
    num_dice, num_sides = get_num_dice_and_sides(ARGV[0])
    die = Die.new(num_sides)
    print 'Rolling ' + num_dice.to_s + 'd' + num_sides.to_s + "...\n"
    roll_dice(num_dice, die)
  elsif ARGV[0] == 'stats'
    roll_new_character
  end
else
  puts 'Please enter your die in the form of: xdy'
  puts 'x: the number of dice to roll'
  puts 'y: the number of sides for each die'
end
