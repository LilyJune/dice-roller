# frozen_string_literal: true

# Class representing a die
class Die
  def initialize(sides)
    @sides = sides
  end

  def roll
    rand(1..@sides)
  end

  def roll_stat
    rolls = []
    (1..4).each do |_i|
      val = roll
      print val.to_s + ', '
      rolls.push(val)
    end
    rolls.reduce(0, :+) - rolls.min
  end
end
